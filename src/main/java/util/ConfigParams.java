package util;

public class ConfigParams {

    public static final String INITIAL_CONTEXT_FACTORY="INITIAL_CONTEXT_FACTORY";
    public static final String PROVIDER_URL="PROVIDER_URL";
    public static final String SECURITY_PRINCIPAL="SECURITY_PRINCIPAL";
    public static final String SECURITY_CREDENTIALS="SECURITY_CREDENTIALS";

}
