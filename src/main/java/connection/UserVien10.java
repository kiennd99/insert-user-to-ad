package connection;
import com.google.gson.JsonObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class UserVien10 {
    private static final Logger LOGGER = Logger.getLogger(UserVien10.class);


    public static String getUser(String token, String url) throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        String result = "";
        try {
            HttpGet request = new HttpGet(url);
            request.addHeader("Authorization", "Bearer " +token);
            request.addHeader("Content-Type", "application/json");
            CloseableHttpResponse response = httpClient.execute(request);
            try {
                HttpEntity httpEntity = response.getEntity();
                if (httpEntity != null) {
                    result = EntityUtils.toString(httpEntity);
                }
            }
            catch (Exception e)
            {
                LOGGER.error(e);
            }
                finally {
                response.close();
            }
        }catch (Exception e)
        {
            LOGGER.error(e);
        }
            finally {
            httpClient.close();
        }
        return result;
    }

    public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        String str=UserVien10.getUser("58c8076f-6bea-3ddf-a41d-987dccf2431c","http://10.110.201.102:8282/hrm/1.0.0/employees?getActiveOnly=true");
        System.out.println(str);

       JSONObject jsonObject=new JSONObject(str);
       JSONObject jsonData= (JSONObject) jsonObject.get("data");
       JSONArray jsonArray=(JSONArray) jsonData.get("units");
       int len=jsonArray.length();
       for(int i=0; i<len; i++)
       {
           JSONObject userOject=(JSONObject)jsonArray.get(i);
           String account =userOject.get("Account").toString();
           String fullName =userOject.get("FullName").toString();
           String email =userOject.get("CompanyEmail").toString();

           System.out.println(account);
           System.out.println(fullName);
           System.out.println(email);
           System.out.println("");
           break;


       }
    }

}
