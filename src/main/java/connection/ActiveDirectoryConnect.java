package connection;

import entity.User;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import util.ConfigParams;
import util.PropsUtil;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.*;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

public class ActiveDirectoryConnect {

    private static final Logger LOGGER = Logger.getLogger(ActiveDirectoryConnect.class);

    private String initialContextFactory = PropsUtil.get(ConfigParams.INITIAL_CONTEXT_FACTORY);
    private String providerUrl= PropsUtil.get(ConfigParams.PROVIDER_URL);
    private String securityPrincipal=PropsUtil.get(ConfigParams.SECURITY_PRINCIPAL);
    private String securityCredentials=PropsUtil.get(ConfigParams.SECURITY_CREDENTIALS);

    public DirContext newConnect()
    {
        DirContext connection=null;
        Properties properties = new Properties();

        properties.put(Context.INITIAL_CONTEXT_FACTORY, initialContextFactory);
        properties.put(Context.PROVIDER_URL, providerUrl);
        properties.put(Context.SECURITY_PRINCIPAL, securityPrincipal);
        properties.put(Context.SECURITY_CREDENTIALS, securityCredentials);

        try
        {
            connection = new InitialDirContext(properties);
            LOGGER.info("Connect ....." + connection);
            return connection;
        }
        catch (Exception e)
        {
            LOGGER.error(e);
        }
        return connection;
    }

    public void closeConnect(DirContext connection)
    {
        try {
            if(connection != null)
                connection.close();
        }
        catch (Exception e) {
           LOGGER.error(e);
        }
    }

    public void getAllUsers() throws NamingException {
        DirContext connection=newConnect();
        try
        {
            String searchFilter = "(objectClass=person)";
            String[] reqAtt = { "cn","givenName","displayName","userPrincipalName","samAccountName","userAccountControl" };
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setReturningAttributes(reqAtt);
            NamingEnumeration users = connection.search("OU=BV110,DC=benhvien110,DC=vn", searchFilter, controls);
            SearchResult result = null;
            int dem=0;
            while (users.hasMore()) {
                result = (SearchResult) users.next();
                Attributes attr = result.getAttributes();
                System.out.println(attr.get("cn"));
                System.out.println(attr.get("givenName"));
                System.out.println(attr.get("displayName"));
                System.out.println(attr.get("samAccountName"));
                System.out.println(attr.get("userPrincipalName"));
                System.out.println(attr.get("userAccountControl"));
                dem++;
            }
            System.out.println(dem);
        }
        catch (Exception e)
        {
            LOGGER.error(e);
        }
        finally {
            closeConnect(connection);
        }

    }

    public void addUser(User user) {
        DirContext connection=newConnect();
        Attributes attributes = new BasicAttributes();
        Attribute attribute = new BasicAttribute("objectClass");
        attribute.add("person");
        attribute.add("top");
        attribute.add("user");
        attribute.add("organizationalPerson");
        attributes.put(attribute);
        attributes.put("cn", user.getUserName());
        attributes.put("sAMAccountName",user.getUserName());
        attributes.put("displayName", user.getFullName());
        attributes.put("userPrincipalName", user.getEmail());
        attributes.put("userPassword",user.getPASSWORD());
        String str="cn="+user.getUserName()+",OU=BV110,DC=benhvien110,DC=vn";
        try {
            connection.createSubcontext(str, attributes);
            LOGGER.info("success...........");
        } catch (Exception e) {
           LOGGER.error(e);
       }
        finally {
            closeConnect(connection);
        }
    }

    public boolean checkUser(String userName) throws NamingException {
        boolean check=false;
        DirContext connection=newConnect();
        try
        {
            String searchFilter = "samAccountName="+userName; // or condition
            String[] reqAtt = { "samAccountName", "cn" };
            SearchControls controls = new SearchControls();
            controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            controls.setReturningAttributes(reqAtt);
            NamingEnumeration users = connection.search("OU=BV110,DC=benhvien110,DC=vn", searchFilter, controls);
            SearchResult result = null;
            while (users.hasMore()) {
                result = (SearchResult) users.next();
                Attributes attr = result.getAttributes();
                String name = attr.get("cn").get(0).toString();
                if(name.equals(userName.toLowerCase())||name.equals(userName))
                {
                    check=true;
                    break;
                }
            }
        }
        catch (Exception e)
        {
            LOGGER.error(e);
        }
        finally {
            closeConnect(connection);
        }

        return check;
    }

    public static void main(String[] args) throws NamingException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
        ActiveDirectoryConnect activeDirectoryConnect=new ActiveDirectoryConnect();
//        String str=UserVien10.getUser("58c8076f-6bea-3ddf-a41d-987dccf2431c","http://10.110.201.102:8282/hrm/1.0.0/employees?getActiveOnly=true");
//
//        JSONObject jsonObject=new JSONObject(str);
//        JSONObject jsonData= (JSONObject) jsonObject.get("data");
//        JSONArray jsonArray=(JSONArray) jsonData.get("units");
//        int len=jsonArray.length();
//        int dem=0;
//        for(int i=0; i<len; i++)
//        {
//            JSONObject userOject=(JSONObject)jsonArray.get(i);
//            String account =userOject.get("Account").toString();
//            String fullName =userOject.get("FullName").toString();
//            String email =userOject.get("CompanyEmail").toString();
//            User user=new User(account,fullName,email);
//            boolean check=activeDirectoryConnect.checkUser(user.getUserName());
//
//            if(check)
//            {
//                dem++;
//                continue;
//
//            }else
//            {
//                System.out.println(account);
//                activeDirectoryConnect.addUser(user);
//                break;
//            }
//        }
        //activeDirectoryConnect.checkUser("AnDTH");
//        if(activeDirectoryConnect.checkUser("AnhNgTM")) System.out.println("ton tai");
//        activeDirectoryConnect.getAllUsers();
        User user=new User("TestTest", "Test Name", "testtest@benhvien110.vn");
        activeDirectoryConnect.addUser(user);
        activeDirectoryConnect.getAllUsers();
    }
}
